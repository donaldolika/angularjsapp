angular.module('app.auth')
.controller('AuthController', ['$location','$scope','$q','LoginService',function($location,$scope,$q,LoginService) {


	 $scope.userInfo=LoginService.getUser();

	$scope.login=function()
	{
		$scope.error = false;
		LoginService.login($scope.logindata).then(function (result) {
                $location.path('/dashboard');
            }, function (error) {
            		$scope.error = true;
                console.log(error);
           });
	};


	$scope.logout=function()
	{
				if(LoginService.logout())
				{
					 	$location.path('/login');
				}
	};

}]);
